# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/12/08 14:38:43 by agrossma          #+#    #+#              #
#    Updated: 2017/12/18 16:54:36 by agrossma         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SHELL		:= /bin/bash

#### Start of system configuration section ####

NAME		:= libftprintf.a
CC			:= gcc
CFLAGS		:= -Wall -Wextra -Werror
AR			:= ar
ARFLAGS		:= rc
RANLIB		:= ranlib
MAKE		:= make
MKDIR		:= mkdir
MKDIRFLAGS	:= -p
RM			:= /bin/rm
RMFLAGS		:= -rf
ECHO		:= echo
QUIET		:= @

#### End of system configuration section ####

#### Start of files definition section ####

SRCDIR		:= src
SRCS		:= \
	ft_printf.c \
	format.c \
	conv_string.c \
	print_padding.c \
	conv_tab.c \
	conv_wstring.c \
	conv_escape.c \
	conv_null.c \
	conv_pointer.c \
	conv_int.c \
	print_int_prefix.c \
	str_nbr_len.c \
	nbr_len.c \
	print_unsigned_int.c \
	ft_putwchar.c \
	conv_char.c \
	conv_wchar.c \
	conv_long.c \
	unsigned_length.c \
	conv_octal.c \
	conv_unsigned.c \
	conv_hex.c
OBJDIR		:= obj
OBJS		:= $(addprefix $(OBJDIR)/, $(SRCS:.c=.o))

#### End of files definition section ####

#### Start of libraries definition section ####

LIBFTDIR	:= libft
LIBFTFUNCS	:= \
	ft_putstr \
	ft_putchar \
	ft_bzero \
	ft_isdigit \
	ft_putstr_fd \
	ft_strchr \
	ft_strlen \
	ft_strnlen \
	ft_putchar_fd \
	ft_memalloc \
	ft_tolower
LIBFTSRC	:= $(addprefix $(SRCDIR)/, $(addsuffix .c, $(LIBFTFUNCS)))
LIBFTOBJS	:= $(addprefix $(OBJDIR)/, $(addsuffix .o, $(LIBFTFUNCS)))

#### End of libraries definition section ####

#### Start of includes definition section ####

INCLUDESDIR	:= includes
IFLAGS		:= -I$(INCLUDESDIR) -Ilibft/$(INCLUDESDIR)

#### End of includes definition section ####

#### Start of linker configuration section ####

LDFLAGS		:= -L$(LIBFTDIR) -l$(LIBFT)

#### End of linker configuration section ####

#### Start of rules section ####

.PHONY: all
all: $(NAME)

$(NAME): $(OBJDIR) $(LIBFTOBJS) $(OBJS)
	$(QUIET)$(ECHO) "Linking the library"
	$(QUIET)$(AR) $(ARFLAGS) $@ $(OBJS) $(LIBFTOBJS)
	$(QUIET)$(ECHO) "Indexing the library"
	$(QUIET)$(RANLIB) $@
	$(QUIET)$(ECHO) "Done."

$(OBJDIR):
	$(QUIET)$(ECHO) "Creating the $@ directory"
	$(QUIET)$(MKDIR) $(MKDIRFLAGS) $(OBJDIR)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(QUIET)$(ECHO) "Compiling $<"
	$(QUIET)$(CC) $(CFLAGS) $(IFLAGS) -c $< -o $@

$(OBJDIR)/%.o: $(LIBFTDIR)/$(SRCDIR)/%.c
	$(QUIET)$(ECHO) "Extracting $(basename $(notdir $@))"
	$(QUIET)$(CC) $(CFLAGS) $(IFLAGS) -c $< -o $(OBJDIR)/$(notdir $@)

.PHONY: clean
clean:
	$(QUIET)$(ECHO) "Cleaning the objects"
	$(QUIET)$(RM) $(RMFLAGS) $(OBJS) $(LIBFTOBJS) $(OBJDIR)

.PHONY: fclean
fclean: clean
	$(QUIET)$(ECHO) "Deleting the library"
	$(QUIET)$(RM) $(RMFLAGS) $(NAME)

.PHONY: re
re: fclean all

#### End of rules section ####
